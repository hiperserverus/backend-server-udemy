// Requires
var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');

// Inicializar variables
var app = express();

//CORS

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
    next();
});

// body parser
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Importar Rutas
var appRoutes = require('./routes/app');
var usuarioRoutes = require('./routes/usuario');
var loginRoutes = require('./routes/login');
var busquedaRoutes = require('./routes/busqueda');
var uploadRoutes = require('./routes/upload');
var imagenesRoutes = require('./routes/imagenes');

var publicacionesroutes = require('./routes/publicacion');
var tiposroutes = require('./routes/tipo');
var subtiposroutes = require('./routes/subtipo');
var detallesroutes = require('./routes/detalle');

// Conexion a la BD
mongoose.connection.openUri('mongodb://localhost:27017/acistft', (err, res) => {
    if (err) throw err;
    console.log('Base de Datos: \x1b[32m%s\x1b[0m', 'online');
});

// Server index config: Esto permite ver el filesistem de las imagenes. Por ejemplo si accedo a http://localhost:3000/uploads/
// en el navegador voy a poder ver toda la estructura de carpetas. No es recomendado para esta app por eso se comenta

// var serveIndex = require('serve-index');
// app.use(express.static(__dirname + '/'))
// app.use('/uploads', serveIndex(__dirname + '/uploads'));


// Rutas
app.use('/usuario', usuarioRoutes);
app.use('/login', loginRoutes);
app.use('/busqueda', busquedaRoutes);
app.use('/upload', uploadRoutes);
app.use('/img', imagenesRoutes);

app.use('/publicacion', publicacionesroutes);
app.use('/tipo', tiposroutes);
app.use('/subtipo', subtiposroutes);
app.use('/detalle', detallesroutes);

app.use('/', appRoutes);


// Escuchar peticiones
app.listen(3000, () => {
    console.log('Express server corriendo en el puerto 3000: \x1b[32m%s\x1b[0m', 'online');
});

/*
var express = require('express');
//var mongoose = require('mongoose');
var bodyParser = require('body-parser');

// Inicializar variables
var app = express();
var methodOverride = require('method-override');
var dao = require('./dao');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());

var router = express.Router();

router.get('/auxiliares', function(request, response) {


    sql = "SELECT codcia, codaux, nomaux, procedencia, clfaux, codiden, nroiden, ruc, diraux, codpais, cod_ubigeo, localidad, contactos, telfnos, faxaux, e_mail, clacli, clapro, claser, clatra, claacc, claesp, claotr, clatrans, flgsit FROM GN_AUXI";
    dao.open(sql, [], false, response);

    response.end


});

app.use(router);

app.listen(3000, function() {
    console.log('Servidor Web - http://localhost:3000');
});
*/