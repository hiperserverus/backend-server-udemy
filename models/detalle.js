var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var detalleSchema = new Schema({
    estado: { type: String },
    ruc: { type: String },
    razonsocial: { type: String },
    seriecorrelativo: { type: String },
    fechaemision: { type: String },
    monto: { type: String },
    id_subtipo: { type: Schema.Types.ObjectId, ref: 'Subtipo', required: [true, 'El id subtipo es un campo obligatorio'] }
});

module.exports = mongoose.model('detalle', detalleSchema);