var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var publicacionSchema = new Schema({
    titulo: { type: String },
    texto: { type: String },
    archivo: { type: String },
    usuario: [{ type: mongoose.Schema.Types.ObjectId, ref: 'usuarios' }]

});

module.exports = mongoose.model('publicacion', publicacionSchema);