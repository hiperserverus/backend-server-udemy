var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var subtipoSchema = new Schema({
    nombre: { type: String, required: [true, 'El nombre es necesario'] },
    id_tipo: { type: Schema.Types.ObjectId, ref: 'tipo', required: [true, 'El id tipo es un campo obligatorio'] }
});

module.exports = mongoose.model('subtipo', subtipoSchema);