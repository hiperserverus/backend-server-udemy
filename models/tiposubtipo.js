var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var tiposubtipoSchema = new Schema({
    id_tipos: [{ type: mongoose.Schema.Types.ObjectId, ref: 'tipos' }],
    id_subtipos: [{ type: mongoose.Schema.Types.ObjectId, ref: 'subtipos' }]
});

module.exports = mongoose.model('tiposubtipo', tiposubtipoSchema);