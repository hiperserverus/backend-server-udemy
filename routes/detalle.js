var express = require('express');
var app = express();
var Detalle = require('../models/detalle');


//================================================
// Listar tipos de facturacion 
//================================================
app.get('/', (req, res, next) => {

    Detalle.find({}).exec(
        (err, detalles) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    mensaje: 'Error cagando detalles',
                    errors: err
                });
            }

            Detalle.count({}, (err, conteo) => {
                res.status(200).json({
                    ok: true,
                    detalles: detalles,
                    total: conteo
                });
            });

        });

});

//================================================
// Crear tipos de facturacion 
//================================================



app.post('/', (req, res) => {

    var body = req.body;

    var detalle = new Detalle({
        estado: body.estado,
        ruc: body.ruc,
        razonsocial: body.razonsocial,
        seriecorrelativo: body.seriecorrelativo,
        fechaemision: body.fechaemision,
        monto: body.monto,
        id_subtipo: body.id_subtipo
    });

    console.log('body:', body);

    detalle.save((err, detalleGuardado) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear detalle',
                errors: err
            });
        }

        res.status(201).json({
            ok: true,
            detalle: detalleGuardado
        });
    });


});

module.exports = app;