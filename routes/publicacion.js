var express = require('express');
var app = express();
var multipart = require('connect-multiparty');
var md_upload = multipart({ uploadDir: './uploads/publicaciones' });
var fs = require('fs');

var Publicacion = require('../models/publicacion');

//Obetner publicaciones

app.get('/', (req, res, next) => {

    Publicacion.find({},
        (err, publicaciones) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    mensaje: 'Error cagando publicaciones',
                    errors: err
                });
            }

            Publicacion.count({}, (err, conteo) => {
                res.status(200).json({
                    ok: true,
                    publicaciones: publicaciones
                });
            });

        });
});


//Crear nuevas publicaciones 

app.post('/', (req, res, next) => {

    var body = req.body;

    if (!body.texto) return res.status(200).send({ message: 'Debes enviar un texto!!' });

    var publicacion = new Publicacion({
        titulo: body.titulo,
        texto: body.texto,
        archivo: null,
        usuario: body.user
    });

    publicacion.save((err, publicacionGuardada) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al guardar publicacion',
                errors: err
            });
        }

        res.status(201).json({
            ok: true,
            publicacion: publicacionGuardada

        });
    });

});

//Subir archivos

app.post('/subir-archivo/:publicacion_id', md_upload, (req, res, next) => {

    var publicacion_id = req.params.publicacion_id;
    console.log(req.params.publicacion_id);
    if (req.files) {

        var file_path = req.files.archivo.path;
        console.log(file_path);

        var file_split = file_path.split('\\');
        console.log(file_split);

        var file_name = file_split[2];
        console.log(file_name);

        var ext_split = file_name.split('\.');
        console.log(ext_split);

        var file_ext = ext_split[1];
        console.log(file_ext);

        Publicacion.findByIdAndUpdate(publicacion_id, { archivo: file_name }, { new: true }, (err, publicacionActualizada) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al guardar publicacion',
                    errors: err
                });
            }

            res.status(201).json({
                ok: true,
                publicacionActualizada: publicacionActualizada

            });
        });
    } else {
        res.status(200).json({
            ok: true,
            mensaje: "No hay archivos para subir",

        });
    }






});

//Obtener archivo

app.get('/archivo', (req, res, next) => {

    var archivo_file = req.params.archivo;
    var path_file = "./uploads/publicaciones/" + archivo_file;

    fs.exists(path_file, (exists) => {

        if (exists) {
            res.sendFile(path.resolve(path_file));
        } else {
            res.status(200).send({ message: 'No existe el archivo solicitado...' });
        }
    });
});



module.exports = app;