var express = require('express');
var app = express();
var Subtipo = require('../models/subtipo');
var Tiposubtipo = require('../models/tiposubtipo');


//================================================
// Listar subtipos de facturacion 
//================================================
app.get('/', (req, res, next) => {

    Subtipo.find({}).exec(
        (err, subtipos) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    mensaje: 'Error cagando tipos',
                    errors: err
                });
            }

            Subtipo.count({}, (err, conteo) => {
                res.status(200).json({
                    ok: true,
                    subtipos: subtipos,
                    total: conteo
                });
            });

        });

});

//================================================
// Crear subtipos de facturacion 
//================================================



app.post('/', (req, res) => {

    var body = req.body;

    var subtipo = new Subtipo({
        nombre: body.nombre,
        id_tipo: body.id_tipo
    });

    console.log('body:', body);

    subtipo.save((err, subtipoGuardado) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear medico',
                errors: err
            });
        }
        //////////////////////////////////////////////////////////////////////////

        var tiposubtipo = new tipoSubtipo({
            id_tipo: subtipoGuardado.id_tipo,
            id_subtipo: subtipoGuardado.id_subtipo

        });
        res.status(201).json({
            ok: true,
            subtipo: tiposubtipo
        });

        /*     subtipo.save((err, subtipoGuardado) => {

                 if (err) {
                     return res.status(400).json({
                         ok: false,
                         mensaje: 'Error al crear medico',
                         errors: err
                     });
                 }
         
                 res.status(201).json({
                     ok: true,
                     subtipo: subtipoGuardado
                 });
             });*/
        //////////////////////////////////////////////////////////////////////////
    });


});

module.exports = app;