var express = require('express');
var app = express();
var Tipo = require('../models/tipo');
var Subtipo = require('../models/subtipo');



//================================================
// Listar tipos de facturacion 
//================================================
app.get('/', (req, res, next) => {

    Tipo.find({}).exec(
        (err, tipos) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    mensaje: 'Error cagando tipos',
                    errors: err
                });
            }

            Tipo.count({}, (err, conteo) => {
                res.status(200).json({
                    ok: true,
                    tipos: tipos,
                    total: conteo
                });
            });

        });

});

//================================================
// Listar tipos de facturacion con sus subtipoos
//================================================
app.get('/subtipos', (req, res, next) => {

    Tipo.find({}).exec(
        (err, tipos) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    mensaje: 'Error cagando tipos',
                    errors: err
                });
            }


            var tipos_clean = [];

            tipos.forEach((tipo) => {
                tipos_clean.push(tipo._id);
            });

            console.log(tipos_clean);


            //////////////////////////////////////////// 

            Subtipo.find({ id_tipo: { "$in": tipos_clean } }).populate('id_tipo').exec(
                (err, tipossubtipos) => {
                    if (err) {
                        return res.status(500).json({
                            ok: false,
                            mensaje: 'Error cagando tipos',
                            errors: err
                        });
                    }

                    res.status(200).json({
                        ok: true,
                        tipossubtipos: tipossubtipos,

                    });

                });

            ////////////////////////////////////////////

        });

});

//================================================
// Crear tipos de facturacion 
//================================================



app.post('/', (req, res) => {

    var body = req.body;

    var tipo = new Tipo({
        nombre: body.nombre
    });

    console.log('body:', body);

    tipo.save((err, tipoGuardado) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear medico',
                errors: err
            });
        }

        res.status(201).json({
            ok: true,
            tipo: tipoGuardado
        });
    });


});

module.exports = app;